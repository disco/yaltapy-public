YALTAPy is a Python toolbox dedicated to the stability analysis of (possibly fractional) delay systems with commensurate delays given by their transfer function.

The questions YALTA and YALTAPy can answer are:

- In the case of neutral systems
    - Find the position of asymptotic axes.
    - If the imaginary axis is asymptotic axis, find if the asymptotic poles of the chain are left or right the imaginary axis.
- In the case of retarded systems or of neutral systems with asymptotic axes in {Re(s) < 0}, find:
    - For a given delay, the number and the position of unstable poles.
    - For which values of the delay the system is stable.
    - For a set of values of the delay, the position of unstable poles (root locus).
    - Find (for non fractional systems) the coprime factors (N, D) of the transfer function as well as an approximation (N<sub>n</sub>, D<sub>n</sub>) in H<sub>∞</sub>-norm